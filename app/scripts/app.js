'use strict';

/**
 * @ngdoc overview
 * @name skyRailsApp
 * @description
 * # skyRailsApp
 *
 * Main module of the application.
 */
angular
    .module('skyRailsApp', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'leaflet-directive',
        'ui.bootstrap.showErrors',
        'ui.tree',
        'angular-bootstrap-select',
        'ui.bootstrap',
        'ui.router'
    ])
    .config(function ($stateProvider, $urlRouterProvider, $sceDelegateProvider, $httpProvider) {

        $httpProvider.interceptors.push('AuthInterceptor');

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('login', {
                url: "/login",
                templateUrl: "views/login.html",
                controller: 'LoginCtrl'
            })
            .state('main', {
                url: "/",
                templateUrl: "views/main.html",
                controller: 'MainCtrl'
            })
            .state('about', {
                url: "/about",
                templateUrl: "views/about.html",
                controller: 'AboutCtrl'
            })
            .state('manualZones', {
                url: "/manualZones",
                templateUrl: "views/manualzones.html",
                controller: 'ManualzonesCtrl'
            })
            .state('rules', {
                url: "/rules",
                templateUrl: "views/rules.html",
                controller: 'RulesCtrl'
            })
            .state('riskmap', {
                url: "/riskmap",
                templateUrl: "views/riskmap.html",
                //controller: 'RiskmapCtrl'
            })
            .state('routeplaner', {
                url: "/routeplaner",
                templateUrl: "views/routeplaner.html",
                controller: 'RouteplanerCtrl'
            })
            .state('drone', {
                url: "/drone",
                templateUrl: "views/drone.html",
                controller: 'DroneCtrl'
            })
            .state('searchandrescue', {
                url: "/searchandrescue",
                templateUrl: "views/searchandrescue.html",
                controller: 'SearchandrescueCtrl'
            })
            .state('simulator', {
                url: "/simulator",
                templateUrl: "views/simulator.html",
                controller: 'SimulatorCtrl'
            })
            .state('setting', {
                url: "/admin",
                templateUrl: "views/admin.html",
                controller: 'AdminCtrl'
            });

        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            'http://104.155.9.154:8080/**',
            'http://130.211.69.229:8080/**',
            'http://localhost:**']);


    })
// run blocks
    .run(function ($rootScope) {
        $rootScope.latLng = {lat: 37.8, lng: -122.45};
        $rootScope.initialZoom = 13;
    });