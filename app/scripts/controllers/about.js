'use strict';

/**
 * @ngdoc function
 * @name skyRailsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the skyRailsApp
 */
angular.module('skyRailsApp')
    .controller('AboutCtrl', ['$scope', 'RestService', '$modal', '$rootScope', function ($scope, RestService, $modal, $rootScope) {
        $scope.setHost = function (isLocal) {
            RestService.setHost(isLocal);
        };
        $scope.disableLogin = function () {

            $rootScope.isAuthenticated = true;

        };
        $scope.updateDebugSelection = function ($event) {
            var checkbox = $event.target;
            if (checkbox.checked){
                $rootScope.debug=true;
            } else {
                $rootScope.debug=false;
            }
            console.log($rootScope.debug);
        };
    }]);
