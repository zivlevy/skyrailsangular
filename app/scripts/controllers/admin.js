'use strict';

/**
 * @ngdoc function
 * @name skyRailsApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the skyRailsApp
 */
angular.module('skyRailsApp')
  .controller('AdminCtrl',['$scope','RestService', '$modal',function ($scope, RestService, $modal) {

    // Drone combo box items
    //manufacturers
    RestService.queryDroneManufacturer('{}').then(
      // good response
      function (response) {
        //insert all to query results array
        $scope.droneManufacturers = response.data;
      },
      //error on response
      function (errorResponse) {
        if (errorResponse.data) {
          $scope.error = errorResponse.data.message;
        }
      });

    //categories
    RestService.getdroneCategories().then(

      // good response
      function (response) {
        console.log(response.data);
        //insert all to query results array
        $scope.droneCategories = response.data;
      },
      //error on response
      function (errorResponse) {
        if (errorResponse.data) {
          $scope.error = errorResponse.data.message;
        }
      });
    //operators
    RestService.getdroneOperators().then(

      // good response
      function (response) {
        console.log(response.data);
        //insert all to query results array
        $scope.droneOperators = response.data;
      },
      //error on response
      function (errorResponse) {
        if (errorResponse.data) {
          $scope.error = errorResponse.data.message;
        }
      });
  }]);
