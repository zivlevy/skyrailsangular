'use strict';

/**
 * @ngdoc function
 * @name skyRailsApp.controller:AreaqueryCtrl
 * @description
 * # AreaqueryCtrl
 * Controller of the skyRailsApp
 */
angular.module('skyRailsApp')
  .controller('AreaqueryCtrl', ['$scope','$window','$modalInstance','query','type',function ($scope,$window,$modalInstance,query,type) {

    //////// Variables ///////
    var fields = [];


    //////////////////////////
    $scope.removeSubItem = function (scope) {
      scope.remove();
    };

    $scope.toggle = function (scope) {
      scope.toggle();
    };

    $scope.update = function (node) {

        if (node.type =='tag' || node.type =='name' ){
          node.regex ='=';
          node.title='';
          node.nodes=[];
        } else if  (node.type =='$and' || node.type =='$or' ){
          delete node.regex;
          delete node.title;
      } else if( node.type = '$all') {
          node.regex = '=';
          node.title= '.';
          node.nodes=[];
      }
    };
    $scope.newSubItem = function (scope) {
      console.log(scope);
      var nodeData = scope.$modelValue;
      nodeData.nodes.push({
        //id: nodeData.id * 10 + nodeData.nodes.length,
        type: 'tag',
        title: '',
        regex: '=',
        nodes: []
      });
    };

    //$scope.collapseAll = function () {
    //  $scope.$broadcast('collapseAll');
    //};
    //
    //$scope.expandAll = function () {
    //  $scope.$broadcast('expandAll');
    //};


    var build = function myself(data) {

      if (data.nodes.length > 0) {
        var start = '';
        var end = '';
        var field = '';
        if (data.type === '$or') {
          start = '{$or:[';
          end = ']}';
        } else if (data.type === '$and') {
          start = '{$and:[';
          end = ']}';
        }
        $scope.query += start;
        for (var i = 0; i < data.nodes.length; i++) {
          myself(data.nodes[i]);

          if (i + 1 < data.nodes.length) {
            $scope.query += ',';
          }
        }
        $scope.query += end;
      } else {
        switch (data.type) {
          case 'tag':
            if (type=='zones') {
              field = 'geoFeature.properties.tags';
            } else {
              field = 'tags';
            }

            break;
          case 'name':
            field = 'name';
            break;
          case '$all':
            field = 'name';
            data.regex = 'startWith';
            break;
          case '$or':
            $scope.err='Illigal Query !'
            break;
          case '$and':
            $scope.err='Illigal Query !'
            break;
        }
        switch (data.regex) {
          case '=':
            $scope.query += "{" + field + ":'" + data.title + "'}";
            break;
          case "startWith":
            $scope.query += "{" + field + ":{$regex:'^" + data.title + "'}}";
            break;
          case "contains":
            $scope.query += "{" + field + ":{$regex:'" + data.title + "'}}";
            break;
          case "endWith":
            $scope.query += "{" + field + ":{$regex:'" + data.title + "$'}}";
            break;
          case "not":
            $scope.query += "{" + field + ":{$not:{$regex:'" + data.title + "'}}}";
            break;
        }
        return;
      }
    }

    var regexQuery = function(str) {
      var re;
      var m;
      //$all
      re = /^\{name:{\$regex:'\^\.'}}/;
      if ((m = re.exec(str)) !== null) {
        return  {"type":"$all","nodes":[],"regex":"startWith","title":"."};
      }

      // $or
      re = /^{\$or:\[(.*)\]}/;
      if ((m = re.exec(str)) !== null) {
        var obj = {"type":"$or","nodes":[]};
        if (m[1]!=""){
          var results =regexQuery(m[1]);
          if (Array.isArray(results)){
            for (var result in results) {
              obj.nodes.push (results[result]);
            }
          }  else {
            obj.nodes.push(results);
          }
        }
        return obj;
      }
      // $and
      re = /^\{\$and:\[(.*)\]\}/;
      if ((m = re.exec(str)) !== null) {
        var obj = {"type":"$and","nodes":[]};
        if (m[1]!=""){
          var results =regexQuery(m[1]);
          if (Array.isArray(results)){
            for (var result in results) {
              obj.nodes.push (results[result]);
            }
          }  else {
            obj.nodes.push(results);
          }
        }
        return obj;
      }

      // multi answer
      re =  /(\{.*?\}),/;
      if ((m = re.exec(str)) !== null) {
        var result = [];
        result.push(regexQuery(m[1]));
        var otherResults = regexQuery(str.slice(m[0].length,str.length));
        if (Array.isArray(otherResults)){
          for (var obj in otherResults) {
            result.push (otherResults[obj]);
          }
        }  else {
          result.push(otherResults);
        }
        return result;
      }

      //the basic token
      var field ='';
      re =  /\{(.*?):(.*)}/;
      if ((m = re.exec(str)) !== null) {
        //convert field names to display field names
        if (type == 'zones' && m[1]=='geoFeature.properties.tags') {
          field = 'tag';
        } else if ( m[1]=='tags') {
          field='tag';
        }else {
          field=m[1];
        }
        //convert eval expresion
        //start with
        var start;
        re =  /\{\$regex:'\^(.*?)'\}/;
        if ((start = re.exec(m[2])) !== null) {

          return {"type":field ,"nodes":[],"regex":"startWith","title":start[1]};
        }

        //end with
        var endWith;
        re =  /\{\$regex:'(.*?)\$'\}/;
        if ((endWith = re.exec(m[2])) !== null) {
          return {"type":field,"nodes":[],"regex":"endWith","title": endWith[1] };

        }
        //not
        var not;
        re =  /\{\$not:\{\$regex:'(.*?)'\}\}/;
        if ((not = re.exec(m[2])) !== null) {
          return {"type":field ,"nodes":[],"regex":"not","title":not[1]};
        }

        //contains
        var contain;
        re =  /\{\$regex:'(.*?)'\}/;
        if ((contain = re.exec(m[2])) !== null) {
          return {"type":field ,"nodes":[],"regex":"contains","title":contain[1]};
        }



        //equal =
        var equal;
        re =  /'(.*)'/;
        if ((equal = re.exec(m[2])) !== null) {
          return {"type": field ,"nodes":[],"regex":"=","title":equal[1]};

        }
        return;
      }
      console.log("err");

    }
    $scope.createQuery = function () {
      $scope.query = "";
      build($scope.data[0]);
    }
//modal functions
    $scope.ok = function () {
      $scope.err='';
      $scope.createQuery();
      var result = {query:$scope.query};

      if ($scope.err =='') {
        $modalInstance.close(result);
      } else {
        $window.alert($scope.err);
      }

    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };


    /////////////////////// init //////////////////////
    // update type related info
    if (type=='rules') {
      $scope.title='Rules';
    } else if (type=='zones'){
      $scope.title='Manual Zones';
    } else if (type=='drones'){
      $scope.title='Drones';
    }

    //turn query recieved from parent form to queryData structure and copy to $scope.data
    $scope.query='';
    console.log(query);
    var temp = regexQuery(query) ;
    temp.root='yes';
    var queryData = [
    ];
    queryData.push(temp);

    $scope.data=angular.copy(queryData);


  }]);
