'use strict';

/**
 * @ngdoc function
 * @name skyRailsApp.controller:DroneCtrl
 * @description
 * # DroneCtrl
 * Controller of the skyRailsApp
 */
angular.module('skyRailsApp')
    .controller('DroneCtrl',['$scope','RestService', 'leafletData','$window', '$modal', '$log',function ($scope, RestService, leafletData,$window, $modal, $log) {
      $scope.query='';
      $scope.queryResultArray=[];
      $scope.scratchPad = null; //holding the new & updated Drone
      var DroneInFocus; // hold the new area or updated area;
      var allLayers = []; //holding all layers
      var visualLayer; //holds all visual, non edited layers
      var riskLayer; //holds risk layer
      var currentEditedLayer; //layer container for new/updated layers
      $scope.isEditOperationInProgress = false;
      $scope.orderByLable='Sort by : Name ';
      $scope.orderBy = 'name';

      ////////////////// View Methods ///////////////////////
      $scope.viewMethods = {

        //Select Drone - Edit Drone
        selectDrone: function(DroneID){
          for (var i in $scope.queryResultArray ){
            if ($scope.queryResultArray[i].id === DroneID){
              $scope.scratchPad=angular.copy($scope.queryResultArray[i]);
              console.log($scope.scratchPad);
              $scope.isEditOperationInProgress=true;
              break;
            }
          }

        },
        //hover Drone - show Drone
        hoverDrone: function(DroneID){
          if ($scope.isEditOperationInProgress) return;
          for (var i in $scope.queryResultArray ){
            if ($scope.queryResultArray[i].id === DroneID){
              $scope.scratchPad=angular.copy($scope.queryResultArray[i]);
              break;
            }
          }

        },

        //mouse move out of Drone - hide Drone
        mouseDownDrone: function(){
          if ($scope.isEditOperationInProgress) return;
          $scope.scratchPad = null;
          $scope.scratchAreas = null;
        },

        //Add new Drone
        addDrone: function(){
          $scope.scratchPad={"isActive":true,"operator":"","name":"","description":"","category":"","manufacturer":"","model":""};
          $scope.isEditOperationInProgress=true;
          $scope.scratchPad.isNew=true;
        },

        //  save
        save: function () {
          console.log('save');
          if ($scope.scratchPad.isNew) {
            RestService.insertDrone($scope.scratchPad).then(function (res) {
              $scope.isEditOperationInProgress = false;
              $scope.scratchPad = null;
              $scope.scratchAreas = null;
              //update Drones list
              DroneMethods.getDrones($scope.query);
            });
          } else {
            RestService.updateDrone($scope.scratchPad.id, $scope.scratchPad).then(function (res) {
              $scope.scratchPad = null;
              $scope.scratchAreas = null;
              $scope.isEditOperationInProgress = false;

              //update Drones list
              DroneMethods.getDrones($scope.query);
            });
          }

        },

        //  cancel
        cancel: function () {

          $scope.scratchPad = null;
          $scope.scratchAreas = null;
          $scope.isEditOperationInProgress=false;
        },


        //  delete
        delete: function () {
          RestService.deleteDrone($scope.scratchPad.id).then(function (res) {
            DroneMethods.getDrones($scope.query);
            $scope.scratchPad = null;
            $scope.scratchAreas = null;
            $scope.isEditOperationInProgress=false;
          });
        },

        //delete tag
        deleteTag: function (index) {
          $scope.scratchPad.tags.splice(index,1);
        },


        //add tag
        addTag: function (index) {
          if ($scope.newTag) {
            if (!$scope.scratchPad.tags){
              $scope.scratchPad.tags=[];
            }
            $scope.scratchPad.tags.push($scope.newTag);
            $scope.newTag="";
          }

        },

        //doQuery
        doQuery: function () {
          DroneMethods.getDrones($scope.query);
        },

        //getOperatorByID
        getOperatorByID: function (operatorId) {
          for (var i=0;i<$scope.droneOperators.length;i++){
            if ($scope.droneOperators[i].id === operatorId) {
              return $scope.droneOperators[i].name;
              break;
            }
          }
        },

        //getManufacturerByID
        getManufacturerByID: function (manufecturerId) {
          for (var i=0;i<$scope.droneManufacturers.length;i++){
            if ($scope.droneManufacturers[i].id === manufecturerId) {
              return $scope.droneManufacturers[i].name;
              break;
            }
          }
        },

        //getCategoryByID
        getCategoryByID: function (categoryId) {
          for (var i=0;i<$scope.droneCategories.length;i++){
            if ($scope.droneCategories[i].id === categoryId) {
              return $scope.droneCategories[i].name;
              break;
            }
          }
        }


      };
      var DroneMethods = {
        getDrones: function (query) {
          //get query or all Drones from backend
          if (!query){
            query='{}';
          }
          RestService.queryDrone(query).then(
            // good response
            function (response) {
              //insert all to query results array
              $scope.queryResultArray=response.data;
            },
            //error on response
            function (errorResponse) {
              if (errorResponse.data) {
                $scope.error = errorResponse.data.message;
              }
            });

        }
      };


      ///////////// Modal for Drones Querybuilder /////////////
      this.animationsEnabled = true;

      //open a modal
      $scope.modalQueryBuilder = function (size, query,queryData) {
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'views/areaquery.html',
          controller: 'AreaqueryCtrl',
          size: size,
          resolve: {
            query: function () {
              return $scope.query;
            },
            type: function(){
              return 'drones';
            }
          }
        });

        modalInstance.result.then(function (result) {
          $scope.query = result.query;
          $scope.viewMethods.doQuery();
        }, function () {

        });
      };




      /////////////////////////////////////
      //initial query
      $scope.query="{name:{$regex:'^.'}}";
      DroneMethods.getDrones($scope.query);

      // Drone combo box items
    //manufacturers
    RestService.queryDroneManufacturer('{}').then(
      // good response
      function (response) {
        //insert all to query results array
        $scope.droneManufacturers = response.data;
      },
      //error on response
      function (errorResponse) {
        if (errorResponse.data) {
          $scope.error = errorResponse.data.message;
        }
      });

    //categories
    RestService.getdroneCategories().then(

      // good response
      function (response) {
        console.log(response.data);
        //insert all to query results array
        $scope.droneCategories = response.data;
      },
      //error on response
      function (errorResponse) {
        if (errorResponse.data) {
          $scope.error = errorResponse.data.message;
        }
      });
    //operators
    RestService.getdroneOperators().then(

      // good response
      function (response) {
        console.log(response.data);
        //insert all to query results array
        $scope.droneOperators = response.data;
      },
      //error on response
      function (errorResponse) {
        if (errorResponse.data) {
          $scope.error = errorResponse.data.message;
        }
      });
    }]);
