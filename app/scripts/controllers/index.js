'use strict';

/**
 * @ngdoc function
 * @name skyRailsApp.controller:IndexctrlCtrl
 * @description
 * # IndexctrlCtrl
 * Controller of the skyRailsApp
 */
angular.module('skyRailsApp')
  .controller('IndexCtrl',['$scope','Auth','$rootScope' ,function ($scope,Auth,$rootScope) {
    $scope.logout = function(){
      Auth.logout();
    };

  }]);
