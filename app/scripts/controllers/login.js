'use strict';

/**
 * @ngdoc function
 * @name skyRailsApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the skyRailsApp
 */
angular.module('skyRailsApp')
  .controller('LoginCtrl', ['$scope','RestService','Auth','$http',function ($scope,RestService,Auth,$http) {
    $scope.username;
    $scope.password;


    $scope.login = function () {
      var credentials = { username: $scope.username, password: $scope.password };
      var credentialsJSON = angular.toJson(credentials);
      Auth.login(credentialsJSON).then(function () {

      },function(error){

        if (error.status == 401){
          $scope.error = 'Username and/or password don\'t match';
        } else {
          $scope.error = error.data.error;
        }


      });

    };


  }]);
