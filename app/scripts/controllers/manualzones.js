'use strict';

/**
 * @ngdoc function
 * @name skyRailsApp.controller:ManualzonesCtrl
 * @description
 * # ManualzonesCtrl
 * Controller of the skyRailsApp
 */
angular.module('skyRailsApp')
  .controller('ManualzonesCtrl', ['$scope','$rootScope', 'RestService', 'leafletData','$window', '$modal', '$log',function ($scope,$rootScope, RestService, leafletData,$window, $modal,$log) {
    $scope.query='';
    $scope.queryResultArray=[];
    $scope.scratchPad = null; //holding the new & updated layer
    var areaInFocus; // hold the new area or updated area;
    var allLayers = []; //holding all layers
    var visualLayer; //holds all visual, non edited layers
    var currentEditedLayer; //layer container for new/updated layers
    $scope.isEditOperationInProgress = false;
    $scope.orderByLable='Sort by : Name ';
    $scope.orderBy = 'name';

    leafletData.getMap().then(function (map) {
      currentEditedLayer = new L.FeatureGroup().addTo(map);
      visualLayer = new L.FeatureGroup().addTo(map);
      //add scale to map
      L.control.scale().addTo(map);

      map.on('draw:drawstart', function () {
        console.log('Draw started');
      });
      map.on('draw:drawstop', function () {
        $scope.isEditOperationInProgress=true;
        console.log('Draw Stoped');
      });
      map.on('draw:created', function (createdItem) {
        console.log('Draw created');
        //clear edited layeer group
        currentEditedLayer.clearLayers();
        var layer = createdItem.layer;
        $scope.scratchPad={};
        $scope.scratchPad.layer = layer;
        $scope.scratchPad.layer.addTo(currentEditedLayer);
        $scope.scratchPad.geoFeature=layer.toGeoJSON();
        $scope.scratchPad.geoFeature.properties ={};
        $scope.scratchPad.layer.editing.enable();
        $scope.scratchPad.isNew = true;
      });
    });



    ////////////////// View Methods ///////////////////////
    $scope.viewMethods = {
      //Select Area - Edit Area
      selecteArea: function(areaID){
        if (!$scope.isEditOperationInProgress) {
          var jsonL = visualLayer.getLayers();
          for (var i in jsonL) {
            if (jsonL[i].feature.properties.id === areaID) {
              var layer = jsonL[i];
              $scope.scratchPad={};
              $scope.scratchPad.layer = layer;
              $scope.scratchPad.layer.addTo(currentEditedLayer);
              $scope.scratchPad.geoFeature=layer.toGeoJSON();
              $scope.scratchPad.layer.editing.enable();
              $scope.scratchPad.isNew = false;
              $scope.isEditOperationInProgress=true;
              $scope.viewMethods.boundArea(areaID);
              break;
            }
          }

        }




      },
      //hover Rule - show Rule
      hoverRule: function(event,areaID){
        if ($scope.isEditOperationInProgress || !event.originalEvent.altKey) return;
        for (var i in $scope.queryResultArray ){
          if ($scope.queryResultArray[i].id === areaID){
            $scope.scratchPad=angular.copy($scope.queryResultArray[i]);
            $scope.viewMethods.boundArea(areaID);
            break;
          }
        }

      },

      //mouse move out of Rule - hide Rule
      mouseDownRule: function(){
        if ($scope.isEditOperationInProgress) return;
        $scope.scratchPad = null;
      },
      //  save
      save: function () {
        //move new layer to visual layers & empty current editable
        $scope.scratchPad.layer.editing.disable();
        var saveItem = {'geoFeature': {}};
        saveItem.geoFeature = $scope.scratchPad.layer.toGeoJSON();
        saveItem.name=$scope.scratchPad.geoFeature.properties.name;
        delete saveItem.geoFeature.properties.name;
        delete saveItem.geoFeature.properties.createdAt;
        delete saveItem.geoFeature.properties.updatedAt;

        saveItem.geoFeature.properties.tags=$scope.scratchPad.geoFeature.properties.tags;

        if ($scope.scratchPad.isNew) {

          RestService.insertArea(saveItem).then(function (res) {
              mapMethods.getMapAreas($scope.query);
              currentEditedLayer.removeLayer($scope.scratchPad.layer);
              $scope.scratchPad=null;
              $scope.isEditOperationInProgress=false;
            });
        } else {
          RestService.updateArea($scope.scratchPad.geoFeature.properties.id,saveItem).then(function (res) {
            mapMethods.getMapAreas($scope.query);
            $scope.scratchPad=null;
            $scope.isEditOperationInProgress=false;
          });
        }
      },

      //  cancel
      cancel: function () {
        console.log('cancel');
        currentEditedLayer.clearLayers();

        if (!$scope.scratchPad.isNew) {
          mapMethods.getMapAreas($scope.query);
        }
        $scope.scratchPad = null;
        $scope.isEditOperationInProgress=false;
        },


      //  delete
      delete: function () {
        RestService.deleteArea($scope.scratchPad.geoFeature.properties.id).then(function (res) {
          mapMethods.getMapAreas($scope.query);
          $scope.scratchPad = null;
          $scope.isEditOperationInProgress=false;
        });
      },

      //delete tag
      deleteTag: function (index) {
        $scope.scratchPad.geoFeature.properties.tags.splice(index,1);
    },


      //add tag
     addTag: function (index) {
       if ($scope.newTag) {
         if (!$scope.scratchPad.geoFeature.properties.tags){
           $scope.scratchPad.geoFeature.properties.tags=[];
         }
         $scope.scratchPad.geoFeature.properties.tags.push($scope.newTag);
         $scope.newTag="";
       }

      },

      //doQuery
      doQuery: function () {
        mapMethods.getMapAreas($scope.query);
      },

      //includeAll - show all areas in query on map
      includeAll: function () {
        leafletData.getMap().then(function (map) {
          //center the map to include all visual areas
          map.fitBounds(visualLayer.getBounds());
        });
      },


      //addTestData - add 100 areas with S&R tag
      addTestData: function(){
        leafletData.getMap().then(function (map) {
          //center the map to include all visual areas
          var mapCenterLat = map.getCenter().lat;
          var mapCenterLng = map.getCenter().lng;

          var A = new google.maps.LatLng(map.getCenter().lat, map.getCenter().lng);
          var pointA = angular.copy(A);
          console.log(pointA);
          var x =10;
          for (var i = 0; i < x; i++) {
            for (var j = 0; j < x; j++) {
              var wpArray = [];
              for (var z = 0; z < 360; z += 15) {

                var radiusInKm = 1;

                var pointB = pointA.destinationPoint(z, 0.1);
                var wp = L.latLng(pointB.lat(), pointB.lng());
                wpArray.push(wp);
              }

              currentEditedLayer.clearLayers();
              var layer = L.polygon(wpArray);
              $scope.scratchPad = {};
              $scope.scratchPad.layer = layer;
              $scope.scratchPad.layer.addTo(currentEditedLayer);
              $scope.scratchPad.geoFeature = layer.toGeoJSON();
              $scope.scratchPad.geoFeature.properties = {'tags': ['S&R','temp'],'name':'test'};
              $scope.scratchPad.layer.editing.enable();
              $scope.scratchPad.isNew = true;
              //add to server
              var saveItem = {'geoFeature': {}};
              saveItem.geoFeature = $scope.scratchPad.layer.toGeoJSON();
              saveItem.name=$scope.scratchPad.geoFeature.properties.name;
              delete saveItem.geoFeature.properties.name;
              delete saveItem.geoFeature.properties.createdAt;
              delete saveItem.geoFeature.properties.updatedAt;

              saveItem.geoFeature.properties.tags=$scope.scratchPad.geoFeature.properties.tags;


                RestService.insertArea(saveItem).then(function (res) {

                });


              /////////////////////////////////////
              pointA = pointA.destinationPoint(90, 0.25);
            }
            pointA = pointA.destinationPoint(180, 0.25);
            pointA = pointA.destinationPoint(270, 0.25*x);
          }


        });
      },

      //bound Area - bound map to area
      boundArea: function(areaID){
        leafletData.getMap().then(function (map) {
          var jsonL = visualLayer.getLayers();
          for (var i in jsonL) {
            if (jsonL[i].feature.properties.id === areaID) {

              //center the map to include all visual areas
              map.fitBounds(jsonL[i].getBounds());
              break;
            }
          }
        });

        }

    };

    var mapMethods = {
      getMapAreas: function (query) {  //initial map query
        //get the map
        leafletData.getMap().then(function (map) {

          //get all areas from backend
          if (!query){
            query='{}';
          }
          RestService.queryArea(query).then(
            // good response
            function (response) {
              //insert all to query results array
              $scope.queryResultArray=response.data;
              //clear current layers from map
              visualLayer.clearLayers();

              //create array with all objects transformed to geoFeature object including external properties in object.properties
              var allObjects = [];
              for (var i = 0; i < response.data.length; i++) {

                if (!response.data[i].geoFeature.properties){
                  response.data[i].geoFeature.properties = {};
                }

                response.data[i].geoFeature.properties.id = response.data[i].id;
                response.data[i].geoFeature.properties.name = response.data[i].name;
                response.data[i].geoFeature.properties.createdAt = response.data[i].createdAt;
                response.data[i].geoFeature.properties.updatedAt = response.data[i].updatedAt;
                allObjects.push(response.data[i].geoFeature);
              }
              //add object to map
              L.geoJson(allObjects, {onEachFeature: onEachFeature});
            },
            //error on response
            function (errorResponse) {
              if (errorResponse.data) {
                $scope.error = errorResponse.data.message;
              }
            });
        });
      }

    };

    function onEachFeature(feature, layer) {
      if (feature.properties) {
        layer.options.fillColor = '#0000ff';
        layer.options.weight = 0;
        layer.options.fillOpacity = 0.3;
        layer.on('mouseover', function (e) {

          if (!$scope.isEditOperationInProgress) {
            //move feature to scratchPad && remove it from current layer
            $scope.scratchPad={};
            $scope.scratchPad.geoFeature=feature;
            $scope.scratchPad.isNew = false;
            $scope.scratchPad.layer = layer;
          }
        });
        layer.on('mouseout', function (e) {
          if (!$scope.isEditOperationInProgress) {
            //move feature to scratchPad && remove it from current layer
            $scope.scratchPad=null;

          }
        });
        layer.on('click', function (e) {
          if (!$scope.isEditOperationInProgress) {
            $scope.isEditOperationInProgress=true;
            //move feature to scratchPad && remove it from current layer
            $scope.scratchPad={};
            $scope.scratchPad.geoFeature=feature;
            $scope.scratchPad.isNew = false;
            $scope.scratchPad.layer = layer;
            //visualLayer.removeLayer(layer);
            $scope.scratchPad.layer.editing.enable();
            $scope.scratchPad.layer.addTo(currentEditedLayer);

          }

        });
        layer.addTo(visualLayer);
      }
    }
    //////////////////  Map init ///////////////////////

      $scope.center = {
        lat: $rootScope.latLng.lat,
        lng: $rootScope.latLng.lng,
        zoom: $rootScope.initialZoom -1
      };
    $scope.layers = {
      baselayers: {
        OpenStreetMap: {
          name: 'Open Streen Map',
          url: 'http://tile.openstreetmap.org/{z}/{x}/{y}.png',
          type: 'xyz',
          layerOptions: {}
        },
        satellite: {
          name: 'Satellite',
          url: 'https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}/256/png8?app_id=cNjOx0DxhSTwsngVa9My&app_code=CQ45DHJ3KvXpHjUoLwjTVg',
          type: 'xyz',
          layerOptions: {}
        },
        grayscale: {
          name: 'Grayscale',
          url: 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
          type: 'xyz',
          layerOptions: {}
        }
      },

      overlays: {}
    };
    $scope.controls =
    {
      position: 'topleft',
      draw: {
        polygon: {
          title: 'Draw a polygon!',
          allowIntersection: false,
          drawError: {
            color: '#b00b00',
            timeout: 1000
          },
          shapeOptions: {
            color: '#0000ee',
            weight: 1
          },
          showArea: false
        },
        polyline: false,
        rectangle: {
          shapeOptions: {
            stroke: true,
            color: '#0000ee',
            weight: 1,
            opacity: 0.5,
            fill: true,
            fillColor: null, //same as color by default
            fillOpacity: 0.2,
            clickable: true
          },
          showArea: false,
          metric: true
        },
        circle: false,
        marker: false
      },
      edit: false

    };

    ///////////// Modal for Querybuilder /////////////
    this.animationsEnabled = true;

    //open a modal
    $scope.modalQueryBuilder = function (size, query,queryData) {
      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'views/areaquery.html',
        controller: 'AreaqueryCtrl',
        size: size,
        resolve: {
          query: function () {
            return $scope.query;
          },
          type: function(){
            return 'zones';
          }
        }
      });

      modalInstance.result.then(function (result) {
        $scope.query = result.query;
        $scope.viewMethods.doQuery();
      }, function () {
        //when cancled
      });
    };
////////////////// Google GeoCoding //////////////

    Number.prototype.toRad = function() {
      return this * Math.PI / 180;
    }

    Number.prototype.toDeg = function() {
      return this * 180 / Math.PI;
    }

    google.maps.LatLng.prototype.destinationPoint = function(brng, dist) {
      dist = dist / 6371;
      brng = brng.toRad();

      var lat1 = this.lat().toRad(), lon1 = this.lng().toRad();

      var lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist) +
        Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));

      var lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(dist) *
          Math.cos(lat1),
          Math.cos(dist) - Math.sin(lat1) *
          Math.sin(lat2));

      if (isNaN(lat2) || isNaN(lon2)) return null;

      return new google.maps.LatLng(lat2.toDeg(), lon2.toDeg());
    }


    $scope.toggleAnimation = function () {
      $scope.animationsEnabled = !$scope.animationsEnabled;
    };




    //initial map query
    $scope.query="{name:{$regex:'^.'}}";
    mapMethods.getMapAreas($scope.query);

  }]);
