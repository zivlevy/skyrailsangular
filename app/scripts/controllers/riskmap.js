'use strict';

/**
 * @ngdoc function
 * @name skyRailsApp.controller:RiskmapCtrl
 * @description
 * # RiskmapCtrl
 * Controller of the skyRailsApp
 */
angular.module('skyRailsApp')
    .controller('RiskmapCtrl', ['$scope', '$rootScope','Auth','$state', 'RestService', 'leafletData', '$window', '$modal', '$log', '$interval', function ($scope, $rootScope, Auth,$state,RestService, leafletData, $window, $modal, $log, $interval) {
        $rootScope.hideHeader = true; //hides header for ios appvar allLayers = []; //holding all layers
        var credentials = {username: 'admin', password: 'admin'};
        var credentialsJSON = angular.toJson(credentials);
        Auth.login(credentialsJSON).then(function () {
            $state.go('riskmap');
            $scope.viewMethods.doQuery();
        }, function (error) {

            if (error.status == 401) {
                $scope.error = 'Username and/or password don\'t match';
            } else {
                $scope.error = error.data.error;
            }


        });
        var riskLayer; //holds risk layer
        var locationMarker ;
        var currentLocation = null;
        $scope.currentDrone = {}; //holding the drone currantly showing risk map
        $scope.query = "{tags:'Hobby'}"; //holding the query string, init to all drones
        $scope.queryResultArray = []; //holding an array of drones that are output of query results

        $scope.isShowHourglass = false; //if hourglass need to be shown on screen
        $scope.isAutoShow = true; //re-get risk map on zoom and move

        $scope.isShowAlert = false; // wheter to show alert box on top
        $scope.alert_title = 'ALERT !'; //the alert box title
        $scope.alert_text = 'Danger in this flight zone !'; //the alert box text
        $scope.alertType = 'close';
        leafletData.getMap().then(function (map) {

            //add risk layer to map
            riskLayer = new L.FeatureGroup().addTo(map);

            //add location manager control
            locationMarker = L.control.locate({
                position: 'topleft',  // set the location of the control
                drawCircle: true,  // controls whether a circle is drawn that shows the uncertainty about the location
                follow: false,  // follow the user's location
                setView: true, // automatically sets the map view to the user's location, enabled if `follow` is true
                keepCurrentZoomLevel: true, // keep the current map zoom level when displaying the user's location. (if `false`, use maxZoom)
                stopFollowingOnDrag: false, // stop following when the map is dragged if `follow` is true (deprecated, see below)
                remainActive: false, // if true locate control remains active on click even if the user's location is in view.
                markerClass: L.circleMarker, // L.circleMarker or L.marker
                circleStyle: {},  // change the style of the circle around the user's location
                markerStyle: {},
                followCircleStyle: {},  // set difference for the style of the circle around the user's location while following
                followMarkerStyle: {},
                icon: 'fa fa-map-marker',  // class for icon, fa-location-arrow or fa-map-marker
                iconLoading: 'fa fa-spinner fa-spin',  // class for loading icon
                circlePadding: [0, 0], // padding around accuracy circle, value is passed to setBounds
                metric: true,  // use metric or imperial units
                onLocationError: function(err) {alert(err.message)},  // define an error callback function
                onLocationOutsideMapBounds:  function(context) { // called when outside map boundaries
                    alert(context.options.strings.outsideMapBoundsMsg);
                },
                showPopup: false, // display a popup when the user click on the inner marker
                strings: {
                    title: "Show me where I am",  // title of the locate control
                    metersUnit: "meters", // string for metric units
                    feetUnit: "feet", // string for imperial units
                    popup: "You are within {distance} {unit} from this point",  // text to appear if user clicks on circle
                    outsideMapBoundsMsg: "You seem located outside the boundaries of the map" // default message for onLocationOutsideMapBounds
                },
                locateOptions: {enableHighAccuracy: true}  // define location options e.g enableHighAccuracy: true or maxZoom: 10
            }).addTo(map);
            //remove zoom control
            map.zoomControl.removeFrom(map);


            //add scale to map
            L.control.scale().addTo(map);

            //add event handlers
            map.on('zoomend', function () {
                if ($scope.isAutoShow) {
                    $scope.viewMethods.getRiskMap(true);
                }

            });
            map.on('moveend', function () {
                if ($scope.isAutoShow) {
                    $scope.viewMethods.getRiskMap(true);
                }
            });

            map.on('locationfound', function (LocationEvent) {
                currentLocation= LocationEvent.latlng;

            });


        });

        var droneLastUpdate =0;
        var rulesLastUpdate=0;
        var zonesLastUpdate=0;
        $scope.callAtInterval = function () {
            if ($scope.isAutoShow==true) {
                RestService.getLastUpdate().then(
                    // good response
                    function (response) {
                        console.log(response);
                        var isUpdateNeeded = false;
                        if (rulesLastUpdate< response.data.ruleLastUpdate || zonesLastUpdate<response.data.zoneLastUpdate){
                            rulesLastUpdate = response.data.ruleLastUpdate;
                            zonesLastUpdate = response.data.zoneLastUpdate;
                            isUpdateNeeded = true;
                        }


                        if (isUpdateNeeded) {
                            $scope.viewMethods.getRiskMap(false);
                        }

                    },

                    //error on response
                    function (errorResponse) {
                        if (errorResponse.data) {
                            $scope.error = errorResponse.data.message;
                        }
                    });
                //find if in closed area
                console.log(locationMarker._marker);
                if (locationMarker._active ) {

                    RestService.getIsPointOpen($scope.currentDrone.id,locationMarker._marker._latlng.lat,locationMarker._marker._latlng.lng).then(
                        // good response
                        function (response) {
                            console.log(response.data);

                            if (response.data.isClosed ==true ){
                                window.location = "myapp:IsOpen:close";
                                $scope.alert_title = 'DANGER !';
                                $scope.alert_text ='NO FLY ZONE !!!';
                                $scope.alertType = 'close';
                                $scope.isShowAlert=true;
                            } else if (response.data.isClosed ==false){
                                if (response.data.riskLevel == 0) {
                                    window.location = "myapp:IsOpen:open";
                                    $scope.isShowAlert=false;
                                } else if (response.data.riskLevel >66) {
                                    window.location = "myapp:IsOpen:danger";
                                    $scope.alert_title = 'DANGER';
                                    $scope.alertType = 'danger';
                                    $scope.alert_text ='Danger level fly zone';
                                    $scope.isShowAlert=true;
                                }else if (response.data.riskLevel >33) {
                                    window.location = "myapp:IsOpen:warning";
                                    $scope.alert_title = 'WARNING';
                                    $scope.alertType = 'warning';
                                    $scope.alert_text ='Warning level fly zone';
                                    $scope.isShowAlert=true;
                                }else if (response.data.riskLevel >0) {
                                    window.location = "myapp:IsOpen:caution";
                                    $scope.alert_title = 'CAUTION';
                                    $scope.alertType = 'caution';
                                    $scope.alert_text ='Caution level fly zone';
                                    $scope.isShowAlert=true;
                                }
                            }

                        },

                        //error on response
                        function (errorResponse) {
                            console.log(response.data);
                            if (errorResponse.data) {
                                $scope.error = errorResponse.data.message;
                                window.location = "myapp://www.error.com/" + errorResponse.data.message;
                            }
                        });

                }

            }
        }

        var getInterval = $interval(function () {
            $scope.callAtInterval();
        }, 1000);

        $scope.$on('$destroy', function() {
            // Make sure that the interval is destroyed too
            $interval.cancel(getInterval);
        });
        ////////////////// View Methods ///////////////////////
        $scope.viewMethods = {

            getRiskMap: function (isShowHourglass) {  //initial map query
                var startDate = new Date();
                //get the map
                leafletData.getMap().then(function (map) {

                    var maxLatLon = map.getBounds().getNorthEast();
                    var minLatLon = map.getBounds().getSouthWest();


                    var mapSize = (geolib.getDistance(minLatLon, maxLatLon));

                    var resolution = 10;
                    if (mapSize < 2000) {
                        resolution = 2;
                    } else if (mapSize < 4000) {
                        resolution = 2;
                    } else if (mapSize < 7000) {
                        resolution = 5;
                    } else if (mapSize < 10000) {
                        resolution = 7;
                    } else if (mapSize < 15000) {
                        resolution = 10;
                    } else if (mapSize < 20000) {
                        resolution = 20;
                    } else if (mapSize < 30000) {
                        resolution = 25;
                    } else if (mapSize < 40000) {
                        resolution = 40;
                    } else if (mapSize < 60000) {
                        resolution = 80;
                    } else if (mapSize < 80000) {
                        resolution = 80;
                    } else if (mapSize < 120000) {
                        resolution = 120;
                    } else if (mapSize < 180000) {
                        resolution = 200;
                    } else {
                        resolution = 300;
                    }
                    if (mapSize > 500000) {
                        $window.alert('Area too big for risk map !');
                        return;
                    }
                    if ($scope.currentDrone == {} || $scope.currentDrone == null) {
                        $window.alert('No valid drone selected !');
                        return;
                    }
                    //show hourglass
                    if (isShowHourglass) {
                        $scope.isShowHourglass = true;
                    }

                    //if we are in the middle of previous call
                    if ($scope.inCall) {
                        return;
                    }
                    $scope.inCall = true;

                    RestService.getRiskMap(resolution, $scope.currentDrone.id, minLatLon.lat, minLatLon.lng, maxLatLon.lat, maxLatLon.lng).then(
                        // good response
                        function (response) {
                            //hide hourglass
                            $scope.isShowHourglass = false;
                            $scope.inCall = false;
                            //clear current layers from map
                            riskLayer.clearLayers();

                            //create array with all objects transformed to geoFeature object including external properties in object.properties
                            var allObjects = response.data.features;
                            console.log(response);
                            console.log(allObjects.length);
                            L.geoJson(allObjects, {onEachFeature: onEachRiskFeature});
                            var endDate = new Date();
                            var time = (endDate - startDate) / 1000;
                            console.log("Total call time:" + time + " sec");
                            riskLayer.bringToBack();
                            window.location = "myapp://www.domain.com/login";
                            console.log('Finished painting riskmap');
                        },
                        //error on response
                        function (errorResponse) {
                            //hide hourglass
                            $scope.isShowHourglass = false;
                            $scope.inCall = false;
                            if (errorResponse.data) {
                                $scope.error = errorResponse.data.message;
                            }
                        });
                });
            },
            toogleAutoShow: function () {
                if ($scope.isAutoShow == false) {
                    $scope.isAutoShow = true;
                    $scope.viewMethods.getRiskMap(true);
                } else {
                    $scope.isAutoShow = false;
                }
            },
            //doQuery
            doQuery: function () {
                DroneMethods.getDrones($scope.query);
            },
            //setCurrentDrone
            setCurrentDrone: function (drone) {
                $scope.currentDrone = drone;
                if ($scope.isAutoShow) {
                    $scope.viewMethods.getRiskMap(true);
                }
            },

        };
        var DroneMethods = {
            getDrones: function (query) {
                //get query or all Drones from backend
                if (!query) {
                    query = '{}';
                }
                RestService.queryDrone(query).then(
                    // good response
                    function (response) {
                        //insert all to query results array
                        $scope.queryResultArray = response.data;
                        $scope.currentDrone = $scope.queryResultArray[0];
                    },

                    //error on response
                    function (errorResponse) {
                        if (errorResponse.data) {
                            $scope.error = errorResponse.data.message;
                        }
                    });

            }
        };

        function onEachRiskFeature(feature, layer) {
            if (feature.properties.isClosed) {
                layer.options.fillColor = '#ff0000'; //Closed
                layer.options.weight = 0;
                layer.options.fillOpacity = 0.8;

            } else {
                layer.options.fillOpacity = 0.5;
                if (feature.properties.riskLevel == 0) {
                    layer.options.fillColor = '#00ff00'; // Open
                    layer.options.fillOpacity = 0.2;
                }
                else if (feature.properties.riskLevel < 33) {
                    layer.options.fillColor = '#e1db37'; //Caution
                } else if (feature.properties.riskLevel < 66) {
                    layer.options.fillColor = '#f5ae14'; // Warning
                } else {
                    layer.options.fillColor = '#ff5c14'; // Danger
                }

                layer.options.weight = 0;

            }
            layer.options.clickable = false;
            layer.addTo(riskLayer);

        }

        //////////////////  Map init ///////////////////////
        $scope.center = {
            lat: 32.115,
            lng: 34.78,
            zoom: 13
        };

        $scope.layers = {
            baselayers: {
                OpenStreetMap: {
                    name: 'Open Streen Map',
                    url: 'http://tile.openstreetmap.org/{z}/{x}/{y}.png',
                    type: 'xyz',
                    layerOptions: {}
                },
                //satellite: {
                //    name: 'Satellite',
                //    url: 'https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}/256/png8?app_id=cNjOx0DxhSTwsngVa9My&app_code=CQ45DHJ3KvXpHjUoLwjTVg',
                //    type: 'xyz',
                //    layerOptions: {}
                //},
                //grayscale: {
                //    name: 'Grayscale',
                //    url: 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
                //    type: 'xyz',
                //    layerOptions: {}
                //}
            },

            overlays: {}
        };
        $scope.controls =
        {
            position: 'topleft',
            draw: false,
            edit: false


        };
        ///////////// Modal for Drones Querybuilder /////////////
        this.animationsEnabled = true;

        //open a modal
        $scope.modalQueryBuilder = function (size, query, queryData) {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'views/areaquery.html',
                controller: 'AreaqueryCtrl',
                size: size,
                resolve: {
                    query: function () {
                        return $scope.query;
                    },
                    type: function () {
                        return 'drones';
                    }
                }
            });

            modalInstance.result.then(function (result) {
                $scope.query = result.query;
                $scope.viewMethods.doQuery();
            }, function () {

            });
        };

        ////////////////////////// main //////////////////////


    }]);

