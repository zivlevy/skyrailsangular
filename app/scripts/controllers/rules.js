'use strict';

/**
 * @ngdoc function
 * @name skyRailsApp.controller:RulesCtrl
 * @description
 * # RulesCtrl
 * Controller of the skyRailsApp
 */
angular.module('skyRailsApp')
  .controller('RulesCtrl',['$scope','$rootScope','RestService', 'leafletData','$window', '$modal', '$log',function ($scope,$rootScope, RestService, leafletData,$window, $modal, $log) {
    $scope.query='';
    $scope.queryResultArray=[];
    $scope.scratchPad = null; //holding the new & updated Rule
    var ruleInFocus; // hold the new area or updated area;
    var allLayers = []; //holding all layers
    var visualLayer; //holds all visual, non edited layers
    var riskLayer; //holds risk layer
    var currentEditedLayer; //layer container for new/updated layers
    $scope.isEditOperationInProgress = false;
    $scope.orderByLable='Sort by : Name ';
    $scope.orderBy = 'name';



    ////////////////// View Methods ///////////////////////
    $scope.viewMethods = {

      //Select Rule - Edit Rule
      selecteRule: function(ruleID){
        for (var i in $scope.queryResultArray ){
          if ($scope.queryResultArray[i].id === ruleID){
            $scope.scratchPad=angular.copy($scope.queryResultArray[i]);
            queryZones($scope.scratchPad.zoneSelect);
            queryDrones($scope.scratchPad.droneSelect);
            $scope.isEditOperationInProgress=true;
            break;
          }
        }

      },
      //hover Rule - show Rule
      hoverRule: function(event,ruleID){
        console.log(event.originalEvent);
        if ($scope.isEditOperationInProgress || !event.originalEvent.altKey) return;
        for (var i in $scope.queryResultArray ){
          if ($scope.queryResultArray[i].id === ruleID){
            $scope.scratchPad=angular.copy($scope.queryResultArray[i]);
            queryZones($scope.scratchPad.zoneSelect);
            queryDrones($scope.scratchPad.droneSelect);
            break;
          }
        }

      },

      //mouse move out of Rule - hide Rule
      mouseDownRule: function(){
        if ($scope.isEditOperationInProgress) return;
        $scope.scratchPad = null;
        $scope.scratchAreas = null;
      },

      //Add new rule
      addRule: function(){
        $scope.scratchDrones=[];
        $scope.scratchAreas=[];
        $scope.scratchPad={"isActive":true,"zoneSelect":"{name:'no name is selected'}","zoneExpand":0,"droneSelect":"{name:'no name is selected'}","type":"Mandatory_Close","riskLevel":0};
        $scope.isEditOperationInProgress=true;
        $scope.scratchPad.isNew=true;
      },

      //  save
      save: function () {
        if ($scope.scratchPad.isNew) {
          RestService.insertRule($scope.scratchPad).then(function (res) {
            $scope.isEditOperationInProgress = false;
            $scope.scratchPad = null;
            $scope.scratchAreas = null;
            //update rules list
            ruleMethods.getRules($scope.query);
          });
        } else {
          RestService.updateRule($scope.scratchPad.id, $scope.scratchPad).then(function (res) {
            $scope.scratchPad = null;
            $scope.scratchAreas = null;
            $scope.isEditOperationInProgress = false;

            //update rules list
            ruleMethods.getRules($scope.query);
          });
        }

      },

      //  cancel
      cancel: function () {

        $scope.scratchPad = null;
        $scope.scratchAreas = null;
        $scope.isEditOperationInProgress=false;
      },


      //  delete
      delete: function () {
        RestService.deleteRule($scope.scratchPad.id).then(function (res) {
          ruleMethods.getRules($scope.query);
          $scope.scratchPad = null;
          $scope.scratchAreas = null;
          $scope.isEditOperationInProgress=false;
        });
      },

      //delete tag
      deleteTag: function (index) {
        $scope.scratchPad.tags.splice(index,1);
      },


      //add tag
      addTag: function (index) {
        if ($scope.newTag) {
          if (!$scope.scratchPad.tags){
            $scope.scratchPad.tags=[];
          }
          $scope.scratchPad.tags.push($scope.newTag);
          $scope.newTag="";
        }

      },

      //doQuery
      doQuery: function () {
        ruleMethods.getRules($scope.query);
      }

    };
    var ruleMethods = {
      getRules: function (query) {
          //get query or all rules from backend
          if (!query){
            query='{}';
          }
          RestService.queryRule(query).then(
            // good response
            function (response) {
              //insert all to query results array
              $scope.queryResultArray=response.data;
            },
            //error on response
            function (errorResponse) {
              if (errorResponse.data) {
                $scope.error = errorResponse.data.message;
              }
            });

      }
    };
/////////////////////  Map Helpers ////////////////////
    leafletData.getMap().then(function (map) {
      currentEditedLayer = new L.FeatureGroup().addTo(map);
      visualLayer = new L.FeatureGroup().addTo(map);
      //add scale to map
      L.control.scale().addTo(map);


    });

    function onEachFeature(feature, layer) {
      if (feature.properties) {
        layer.options.fillColor = '#0000ff';
        layer.options.weight = 0;
        layer.options.fillOpacity = 0.3;
        //layer.on('mouseover', function (e) {
        //  if (!$scope.isEditOperationInProgress) {
        //    //move feature to scratchPad && remove it from current layer
        //    $scope.scratchPad={};
        //    $scope.scratchPad.geoFeature=feature;
        //    $scope.scratchPad.isNew = false;
        //    $scope.scratchPad.layer = layer;
        //  }
        //});
        //layer.on('mouseout', function (e) {
        //  if (!$scope.isEditOperationInProgress) {
        //    //move feature to scratchPad && remove it from current layer
        //    $scope.scratchPad=null;
        //
        //  }
        //});
        //layer.on('click', function (e) {
        //  if (!$scope.isEditOperationInProgress) {
        //    $scope.isEditOperationInProgress=true;
        //    //move feature to scratchPad && remove it from current layer
        //    $scope.scratchPad={};
        //    $scope.scratchPad.geoFeature=feature;
        //    $scope.scratchPad.isNew = false;
        //    $scope.scratchPad.layer = layer;
        //    //visualLayer.removeLayer(layer);
        //    $scope.scratchPad.layer.editing.enable();
        //    $scope.scratchPad.layer.addTo(currentEditedLayer);
        //
        //  }
        //
        //});
        layer.addTo(visualLayer);
      }
    }

    var boundAll = function(){
      //boundAll - show all areas in query on map
        leafletData.getMap().then(function (map) {
          //center the map to include all visual areas
          map.fitBounds(visualLayer.getBounds());
        });
      }

    //////////////////  Map init ///////////////////////
      $scope.center = {
        lat: $rootScope.latLng.lat,
        lng: $rootScope.latLng.lng,
        zoom: $rootScope.initialZoom -1
      };

    $scope.layers = {
      baselayers: {
        OpenStreetMap: {
          name: 'Open Streen Map',
          url: 'http://tile.openstreetmap.org/{z}/{x}/{y}.png',
          type: 'xyz',
          layerOptions: {}
        },
        satellite: {
          name: 'Satellite',
          url: 'https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}/256/png8?app_id=cNjOx0DxhSTwsngVa9My&app_code=CQ45DHJ3KvXpHjUoLwjTVg',
          type: 'xyz',
          layerOptions: {}
        },
        grayscale: {
          name: 'Grayscale',
          url: 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
          type: 'xyz',
          layerOptions: {}
        }
      },

      overlays: {}
    };
    $scope.controls =
    {
      position: 'topleft',
      draw: {
        polygon: {
          title: 'Draw a polygon!',
          allowIntersection: false,
          drawError: {
            color: '#b00b00',
            timeout: 1000
          },
          shapeOptions: {
            color: '#0000ee',
            weight: 1
          },
          showArea: false
        },
        polyline: false,
        rectangle: {
          shapeOptions: {
            stroke: true,
            color: '#0000ee',
            weight: 1,
            opacity: 0.5,
            fill: true,
            fillColor: null, //same as color by default
            fillOpacity: 0.2,
            clickable: true
          },
          showArea: false,
          metric: true
        },
        circle: false,
        marker: false
      },
      edit: false

    };

    ///////////// Modal for Rules Querybuilder /////////////
    this.animationsEnabled = true;

    //open a modal
    $scope.modalQueryBuilder = function (size, query,queryData) {
      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'views/areaquery.html',
        controller: 'AreaqueryCtrl',
        size: size,
        resolve: {
          query: function () {
            return $scope.query;
          },
          type: function(){
            return 'rules';
          }
        }
      });

      modalInstance.result.then(function (result) {
        $scope.query = result.query;
        $scope.viewMethods.doQuery();
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    ///////////// Modal for Area Querybuilder /////////////

    //open a modal
    $scope.areaModalQueryBuilder = function (size, query,queryData) {
      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'views/areaquery.html',
        controller: 'AreaqueryCtrl',
        size: size,
        resolve: {
          query: function () {
            return $scope.scratchPad.zoneSelect;
          },
        type: function(){
          return 'zones';
        }
        }
      });

      modalInstance.result.then(function (result) {
        $scope.scratchPad.zoneSelect = result.query;
        queryZones(result.query);
      }, function () {
      });
    };

    ///////////// Modal for Drone Querybuilder /////////////

    //open a modal
    $scope.droneModalQueryBuilder = function (size, query,queryData) {
      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'views/areaquery.html',
        controller: 'AreaqueryCtrl',
        size: size,
        resolve: {
          query: function () {

            return $scope.scratchPad.droneSelect;
          },
          type: function(){
            return 'drones';
          }
        }
      });

      modalInstance.result.then(function (result) {
        $scope.scratchPad.droneSelect = result.query;
        queryDrones(result.query);
      }, function () {
      });
    };
    $scope.toggleAnimation = function () {
      $scope.animationsEnabled = !$scope.animationsEnabled;
    };

    //////////////// Support methods/////////////////////
    var queryZones=function(query) {
      RestService.queryArea(query).then(
        // good response
        function (response) {
          //insert all to query results array
          $scope.scratchAreas=response.data;
          //clear current layers from map
          visualLayer.clearLayers();
          //
          ////create array with all objects transformed to geoFeature object including external properties in object.properties
          var allObjects = [];
          for (var i = 0; i < response.data.length; i++) {

            if (!response.data[i].geoFeature.properties){
              response.data[i].geoFeature.properties = {};
            }

            response.data[i].geoFeature.properties.id = response.data[i].id;
            response.data[i].geoFeature.properties.name = response.data[i].name;
            response.data[i].geoFeature.properties.createdAt = response.data[i].createdAt;
            response.data[i].geoFeature.properties.updatedAt = response.data[i].updatedAt;
            allObjects.push(response.data[i].geoFeature);
          }
          //add object to map
          L.geoJson(allObjects, {onEachFeature: onEachFeature});
          if (allObjects.length >0){
            boundAll();
          }

        },
        //error on response
        function (errorResponse) {
          if (errorResponse.data) {
            $scope.error = errorResponse.data.message;
          }
        });
    }

    var queryDrones=function(query) {
      RestService.queryDrone(query).then(
        // good response
        function (response) {
          //insert all to query results array
          $scope.scratchDrones=response.data;
        },
        //error on response
        function (errorResponse) {
          if (errorResponse.data) {
            $scope.error = errorResponse.data.message;
          }
        });
    }
    /////////////////////////////////////
    //initial map query
    $scope.query="{name:{$regex:'^.'}}";
    ruleMethods.getRules($scope.query);


  }]);
