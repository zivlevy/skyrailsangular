'use strict';

/**
 * @ngdoc function
 * @name skyRailsApp.controller:RouteplanerCtrl
 * @description
 * # RouteplanerCtrl
 * Controller of the skyRailsApp
 */

angular.module('skyRailsApp')
    .controller('SimulatorCtrl', ['$scope', '$rootScope','RestService', 'leafletData', '$window', '$modal', '$log', '$interval','sim', function ($scope, $rootScope,RestService, leafletData, $window, $modal, $log, $interval,sim) {
      var allLayers = []; //holding all layers
      var riskLayer; //holds risk layer
      var routeLayer; //holds risk layer
      var debugLayer; //holds debug info

      var droneRoute; //holds the calculated route for a drone
      $scope.inFlight = false; //true when flight simulator is on

      $scope.routeStartTitle = ""; //route start title
      $scope.routeEndTitle=""; //route end title

      $scope.currentDrone = {}; //holding the drone currantly showing risk map
      $scope.query="{name:{$regex:'^.'}}"; //holding the query string, init to all drones
      $scope.queryResultArray=[]; //holding an array of drones that are output of query results

      $scope.isShowHourglass = false; //if hourglass need to be shown on screen
      $scope.isAutoShow = false; //re-get risk map on zoom and move

      var simUpdateCallback = function(currentLocation){
        console.log("from callback");
        $scope.markers.simMarker.lat=currentLocation.latitude;
        $scope.markers.simMarker.lng=currentLocation.longitude;
      };

      var simRouteChangedCallback = function(newRoute){
        console.log("from simRouteChangedCallback");
        //get the map
        leafletData.getMap().then(function (map) {

                //clear current layers from map
                routeLayer.clearLayers();
                debugLayer.clearLayers();
                //create array with all objects transformed to geoFeature object including external properties in object.properties
                var allObjects = newRoute;
                L.geoJson(allObjects, {onEachFeature: onEachRouteFeature});
        });
      };

      leafletData.getMap().then(function (map) {
        riskLayer = new L.FeatureGroup().addTo(map);
        routeLayer = new L.FeatureGroup().addTo(map);
        debugLayer = new L.FeatureGroup().addTo(map);

        //add scale to map
        L.control.scale().addTo(map);

        map.on('zoomend', function () {
          if ($scope.isAutoShow) {
            $scope.viewMethods.getRiskMap(true);
          }

        });
        map.on('moveend', function () {
          if ($scope.isAutoShow) {
            $scope.viewMethods.getRiskMap(true);
          }
        });
        map.on('click', function ($event) {
          console.log($event);
          if ($event.originalEvent.altKey && !$event.originalEvent.shiftKey){
            //move route start
            $scope.markers.startMarker.lat=$event.latlng.lat;
            $scope.markers.startMarker.lng=$event.latlng.lng;

            //clear route layer
            $scope.viewMethods.clearRoute();

            //update title
            $scope.routeStartTitle='';
            codeLatLng ($scope.markers.startMarker.lat,$scope.markers.startMarker.lng,'routeStartTitle');


          }else if ($event.originalEvent.shiftKey && $event.originalEvent.altKey ){
            //move route end
            $scope.markers.endMarker.lat=$event.latlng.lat;
            $scope.markers.endMarker.lng=$event.latlng.lng;

            //clear route layer
            $scope.viewMethods.clearRoute();

            //update title
            $scope.routeEndTitle='';
            codeLatLng ($scope.markers.endMarker.lat,$scope.markers.endMarker.lng,'routeEndTitle');

          }
        });
      });

      ////////// auto show interval ////////////////
      var droneLastUpdate =0;
      var rulesLastUpdate=0;
      var zonesLastUpdate=0;
      $scope.callAtInterval = function () {
        if ($scope.isAutoShow==true) {
          RestService.getLastUpdate().then(
              // good response
              function (response) {
                console.log(response);
                var isUpdateNeeded = false;
                if (rulesLastUpdate< response.data.ruleLastUpdate || zonesLastUpdate<response.data.zoneLastUpdate){
                  rulesLastUpdate = response.data.ruleLastUpdate;
                  zonesLastUpdate = response.data.zoneLastUpdate;
                  isUpdateNeeded = true;
                }


                if (isUpdateNeeded) {
                  if (!$scope.inFlight){
                    $scope.viewMethods.getRoute(false);
                  }
                  $scope.viewMethods.getRiskMap(false);
                }

              },

              //error on response
              function (errorResponse) {
                if (errorResponse.data) {
                  $scope.error = errorResponse.data.message;
                }
              });
        }
      };

      var getInterval = $interval(function () {
        $scope.callAtInterval();
      }, 1000);

      $scope.$on('$destroy', function() {
        // Make sure that the interval is destroyed too
        $interval.cancel(getInterval);
      });

      ////////////////// View Methods ///////////////////////
      $scope.viewMethods = {

        getRiskMap: function (isShowHourglass) {  //initial map query
          var startDate = new Date();
          //get the map
          leafletData.getMap().then(function (map) {

            var maxLatLon = map.getBounds().getNorthEast();
            var minLatLon = map.getBounds().getSouthWest();


            var mapSize = (geolib.getDistance(minLatLon, maxLatLon));

            var resolution = 10;
            if (mapSize < 2000) {
              resolution = 2;
            } else if (mapSize < 4000) {
              resolution = 5;
            } else if (mapSize < 7000) {
              resolution = 7;
            } else if (mapSize < 10000) {
              resolution = 7;
            } else if (mapSize < 15000) {
              resolution = 10;
            } else if (mapSize < 20000) {
              resolution = 20;
            } else if (mapSize < 30000) {
              resolution = 25;
            } else if (mapSize < 40000) {
              resolution = 40;
            } else if (mapSize < 60000) {
              resolution = 80;
            } else if (mapSize < 80000) {
              resolution = 80;
            } else if (mapSize < 120000) {
              resolution = 120;
            } else if (mapSize < 180000) {
              resolution = 200;
            } else {
              resolution = 300;
            }
            if (mapSize > 1000000) {
              $window.alert('Area too big for risk map !');
              return;
            }
            if ($scope.currentDrone =={} || $scope.currentDrone==null){
              $window.alert('No valid drone selected !');
              return;
            }
            //show hourglass
            if (isShowHourglass) {
              $scope.isShowHourglass = true;
            }

            //if we are in the middle of previous call
            if ($scope.inCallRiskmap) {
              return;
            }
            $scope.inCallRiskmap = true;

            RestService.getRiskMap(resolution, $scope.currentDrone.id, minLatLon.lat, minLatLon.lng, maxLatLon.lat, maxLatLon.lng).then(
                // good response
                function (response) {
                  //hide hourglass
                  $scope.isShowHourglass = false;
                  $scope.inCallRiskmap = false;
                  //clear current layers from map
                  riskLayer.clearLayers();

                  //create array with all objects transformed to geoFeature object including external properties in object.properties
                  var allObjects = response.data.features;
                  console.log(response);
                  console.log(allObjects.length);
                  L.geoJson(allObjects, {onEachFeature: onEachRiskFeature});
                  var endDate = new Date();
                  var time = (endDate-startDate)/1000;
                  console.log("Total call time:" + time + " sec");
                },
                //error on response
                function (errorResponse) {
                  //hide hourglass
                  $scope.isShowHourglass = false;
                  $scope.inCallRiskmap = false;
                  if (errorResponse.data) {
                    $scope.error = errorResponse.data.message;
                  }
                });
          });
        },
        getRoute: function (isShowHourglass) {
          var startDate = new Date();
          //get the map
          leafletData.getMap().then(function (map) {
            var startMarker = $scope.markers.startMarker;
            var endMarker = $scope.markers.endMarker;

            //show hourglass
            if (isShowHourglass) {
              $scope.isShowHourglass = true;
            }

            //if we are in the middle of previous call
            if ($scope.inCall) {
              return;
            }
            $scope.inCall = true;
            var resolution = 30;
            RestService.getRoute (resolution, $scope.currentDrone.id, $scope.markers.startMarker.lat, $scope.markers.startMarker.lng, $scope.markers.endMarker.lat, $scope.markers.endMarker.lng).then(
                // good response
                function (response) {

                  //hide hourglass
                  $scope.isShowHourglass = false;
                  $scope.inCall = false;
                  //clear current layers from map
                  routeLayer.clearLayers();
                  debugLayer.clearLayers();
                  $scope.routeErrorMessage=null;
                  //route data
                  $scope.routeExists=true;
                  $scope.routeDistance = response.data.properties.optimizedWeightedDistance;
                  $scope.routeDirectDistance = response.data.properties.straightLineDistance;
                  $scope.routeNoOfWaypoints = response.data.geometry.coordinates.length;
                  //create array with all objects transformed to geoFeature object including external properties in object.properties
                  var allObjects = response.data;
                  droneRoute = allObjects;
                  if ($rootScope.debug) {
                    //add markers to each point
                    for (var i = 0; i < allObjects.geometry.coordinates.length; i++) {
                      var newMarker = new L.marker([allObjects.geometry.coordinates[i][1], allObjects.geometry.coordinates[i][0]], {
                            draggable: false,        // Make the icon dragable
                            title: i,     // Add a title
                            opacity: 1
                          }// Adjust the opacity
                      );


                      newMarker.addTo(debugLayer);
                    }
                  }

                  L.geoJson(allObjects, {onEachFeature: onEachRouteFeature});
                  var endDate = new Date();
                  var time = (endDate-startDate)/1000;
                  console.log("Total call time:" + time + " sec");
                  $scope.routeCalculationTime = time;
                },
                //error on response
                function (errorResponse) {
                  //clear route layer
                  $scope.viewMethods.clearRoute();

                  //show error meaasge
                  $scope.routeErrorMessage='Could not find leagal route';
                  //hide hourglass
                  $scope.isShowHourglass = false;
                  $scope.inCall = false;
                  if (errorResponse.data) {
                    $scope.error = errorResponse.data.message;
                  }
                });
          });
        },
        //start flight sim
        fly:function(){
          $scope.inFlight = true;
          $scope.markers.startMarker.draggable=false;
          $scope.markers.endMarker.draggable=false;
          sim.fly(droneRoute.geometry.coordinates.slice(), $scope.currentDrone,simUpdateCallback,simRouteChangedCallback);
          $scope.markers.simMarker.opacity = 1.0;
        },
        //stop fligt sim
        stopFly: function(){
          $scope.inFlight = false;
          $scope.markers.startMarker.draggable=true;
          $scope.markers.endMarker.draggable=true;
          $scope.routeExists=false;
          $scope.markers.simMarker.opacity = 0.0;
          $scope.markers.simMarker.lat=0;
          $scope.markers.simMarker.lon=0;
          sim.stopFly();
        },
        toogleAutoShow: function () {
          if ($scope.isAutoShow == false) {
            $scope.isAutoShow = true;
            $scope.viewMethods.getRiskMap(true);
          } else {
            $scope.isAutoShow = false;
          }
        },
        //doQuery
        doQuery: function () {
          DroneMethods.getDrones($scope.query);
        },
        //includeAll - show both start and end points on map
        includeAll: function () {
          leafletData.getMap().then(function (map) {
            //center the map to include all visual areas
            map.fitBounds([[$scope.markers.startMarker.lat,$scope.markers.startMarker.lng],
              [$scope.markers.endMarker.lat,$scope.markers.endMarker.lng]]);
          });
        },
        //setCurrentDrone
        setCurrentDrone: function (drone) {
          $scope.currentDrone = drone;
          if ($scope.isAutoShow) {
            $scope.viewMethods.getRiskMap(true);
            $scope.viewMethods.getRoute(true);
          }
        },
        //startAddressSet - callback for address directive
        startAddressSet:function(latLng){
          console.log(latLng);
          $scope.markers.startMarker.lat=latLng.lat();
          $scope.markers.startMarker.lng=latLng.lng();
          $scope.viewMethods.includeAll();
          //clear route layer
          $scope.viewMethods.clearRoute();
        },

        //endAddressSet - callback for address directive
        endAddressSet:function(latLng){
          $scope.markers.endMarker.lat=latLng.lat();
          $scope.markers.endMarker.lng=latLng.lng();
          $scope.viewMethods.includeAll();
          //clear route layer
          $scope.viewMethods.clearRoute();
        },
        //clearRoute - clears route
        clearRoute:function(){
          routeLayer.clearLayers();
          debugLayer.clearLayers();
          $scope.routeExists = false;
          $scope.routeErrorMessage = null;
        },
        //clearRiskMap - clears risk map
        clearRiskMap:function(){
          riskLayer.clearLayers();
        },


      };
      var DroneMethods = {
        getDrones: function (query) {
          //get query or all Drones from backend
          if (!query){
            query='{}';
          }
          RestService.queryDrone(query).then(
              // good response
              function (response) {
                //insert all to query results array
                $scope.queryResultArray=response.data;
                $scope.currentDrone =  $scope.queryResultArray[0];
              },

              //error on response
              function (errorResponse) {
                if (errorResponse.data) {
                  $scope.error = errorResponse.data.message;
                }
              });

        }
      };
      function onEachRiskFeature(feature, layer) {
        if (feature.properties.isClosed) {
          layer.options.fillColor = '#ff0000'; //Closed
          layer.options.weight = 0;
          layer.options.fillOpacity = 0.8;

        } else {
          layer.options.fillOpacity = 0.5;
          if (feature.properties.riskLevel == 0) {
            layer.options.fillColor = '#00ff00'; //Open
            layer.options.fillOpacity = 0.0;
          }
          else if (feature.properties.riskLevel < 33) {
            layer.options.fillColor = '#e1db37'; //Caution
          } else if (feature.properties.riskLevel < 66) {
            layer.options.fillColor = '#f5ae14'; // Warning
          } else {
            layer.options.fillColor = '#ff5c14'; // Danger
          }

          layer.options.weight = 0;

        }
        layer.options.clickable = false;
        layer.addTo(riskLayer);
      }


      // route
      function onEachRouteFeature(feature, layer) {

        layer.options.color = '#0000ff';
        layer.options.weight = 4.0;
        layer.options.fillOpacity = 0.8;

        layer.addTo(routeLayer);
      }
      //////////////////  Map init ///////////////////////
      $scope.center = {
        lat: $rootScope.latLng.lat,
        lng: $rootScope.latLng.lng,
        zoom: $rootScope.initialZoom
      };

      $scope.layers = {
        baselayers: {
          OpenStreetMap: {
            name: 'Open Streen Map',
            url: 'http://tile.openstreetmap.org/{z}/{x}/{y}.png',
            type: 'xyz',
            layerOptions: {}
          },
          satellite: {
            name: 'Satellite',
            url: 'https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}/256/png8?app_id=cNjOx0DxhSTwsngVa9My&app_code=CQ45DHJ3KvXpHjUoLwjTVg',
            type: 'xyz',
            layerOptions: {}
          },
          grayscale: {
            name: 'Grayscale',
            url: 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
            type: 'xyz',
            layerOptions: {}
          },
          mapBox: {
            name: 'Dark',
            url: 'https://api.mapbox.com/v4/mapbox.dark/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoieml2bGV2eSIsImEiOiJwaEpQeUNRIn0.OZupy_Vjyl5eRCRlgV6otg',
            type: 'xyz',
            layerOptions: {}
          }
        },

        overlays: {}
      };
      $scope.markers = {
        startMarker: {
          icon: {
            type: 'awesomeMarker',
            icon: 'home',
            markerColor: 'green'
          },
          lat: 0,
          lng: 0,
          draggable: true
        },
        endMarker:{

          icon: {
            type: 'awesomeMarker',
            icon: 'star',
            markerColor: 'red'
          },
          lat: 0,
          lng: 0,
          draggable: true

        },
        simMarker:{

          icon: {
            type: 'awesomeMarker',
            icon: 'star',
            markerColor: 'blue'
          },
          lat: 0,
          lng: 0,
          draggable: true
        }

      }
      $scope.controls =
      {
        position: 'topleft',
        draw: false,
        edit: false

      };

      ///////////// map events ////////////////////////////////////////
      $scope.$on('leafletDirectiveMarker.dragend', function (e, args) {
        var currentMarker ;
        if (args.modelName == 'startMarker') {
          currentMarker = $scope.markers.startMarker;
          $scope.routeStartTitle='';
          codeLatLng ($scope.markers.startMarker.lat,$scope.markers.startMarker.lng,'routeStartTitle');
        } else if (args.modelName == 'endMarker') {
          currentMarker = $scope.markers.endMarker;
          $scope.routeEndTitle='';
          codeLatLng ($scope.markers.endMarker.lat,$scope.markers.endMarker.lng,'routeEndTitle');

        }
        currentMarker.lat=args.model.lat;
        currentMarker.lng=args.model.lng;

        $scope.viewMethods.clearRoute();
      });

      ///////////// Modal for Drones Querybuilder /////////////
      this.animationsEnabled = true;

      //open a modal
      $scope.modalQueryBuilder = function (size, query,queryData) {
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'views/areaquery.html',
          controller: 'AreaqueryCtrl',
          size: size,
          resolve: {
            query: function () {
              return $scope.query;
            },
            type: function(){
              return 'drones';
            }
          }
        });

        modalInstance.result.then(function (result) {
          $scope.query = result.query;
          $scope.viewMethods.doQuery();
        }, function () {

        });
      };

      /////////////////////// google geocoder //////////////
      var codeLatLng = function codeLatLng(lat, lng, target) {
        var geocoder;
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(lat, lng);
        geocoder.geocode({'latLng': latlng}, function (results, status) {
          var msg = '';
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              msg = (results[0].formatted_address);
            } else {
              msg = ('No results found');
            }
          } else {
            msg = ('Geocoder failed due to: ' + status);
          }

          //update ui
          if (target == 'routeStartTitle') {
            $scope.routeStartTitle = msg;
          } else if (target == 'routeEndTitle') {
            $scope.routeEndTitle = msg;
          }
          $scope.$apply();
        });
      }
      ////////////////////////// main //////////////////////
      $scope.viewMethods.doQuery();

    }]);


