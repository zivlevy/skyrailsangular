'use strict';

/**
 * @ngdoc directive
 * @name skyRailsApp.directive:addressSearch
 * @description
 * # addressSearch
 */
angular.module('skyRailsApp')
  .directive('zlAddress', function () {
    var controller = function ($scope,$timeout) {
      //var $scope = this;
      ////////////////// Google GeoCoding //////////////
      var geoFlag = false; //when rised don't call google geoCoder
      var delayedFlag = false; //if there is allready one delay
      var geocoder;
      geocoder = new google.maps.Geocoder();

      $scope.getAddressList = function (){
        if ($scope.inputAddress == '' || $scope.inputAddress == null){
          $scope.addressResultList = null;
          return;
        }
        if (!geoFlag){
          geoFlag = true;
          if ($scope.inputAddress != '' || $scope.inputAddress != null) {
            geocoder.geocode( { 'address': $scope.inputAddress,    componentRestrictions: {
              country: 'IL'
            }}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                //add results to list
                $scope.addressResultList = results;
                $scope.$apply();
              } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT){
                if (!delayedFlag){
                  delayedFlag=true;
                  $timeout(executeAfterPeriod, 250);
                }
              } else {
                $scope.addressResultList = null;

              }

            });
          } else {
            $scope.addressResultList = null;
          }
        } else {
          if (!delayedFlag){
            delayedFlag=true;
            $timeout(executeAfterPeriod, 250);
          }
        }
      };
      var executeAfterPeriod = function (){

        geoFlag = false;
        delayedFlag=false;
        $scope.getAddressList();
      };
      $scope.selectAddress = function(addressObject) {
        $scope.inputAddress=addressObject.formatted_address;
        $scope.addressResultList = null;
        var point = new google.maps.LatLng(addressObject.geometry.location.lat(), addressObject.geometry.location.lng());
        $scope.callback({latLng:point});
        $scope.title = addressObject.formatted_address;
      };


      //////////////////////////////////////
    };

    return {
      templateUrl: 'views/directiveViews/addresssearch.html',
      scope: {
        callback:'&',
        title:'='
      },
      controller: controller,
      restrict: 'E',
      link: function($scope) {
        $scope.$watch('title', function(newValue, oldValue, scope) {
          scope.inputAddress = newValue;


        });
      }
    };
  });
