'use strict';

/**
 * @ngdoc service
 * @name skyRailsApp.Auth
 * @description
 * # Auth
 * Service in the skyRailsApp.
 */
angular.module('skyRailsApp')
.factory('Auth',['$http','RestService','$rootScope','$state', function($http,RestService,$rootScope,$state) {
  return {
    authorize: function(access) {
      return this.isAuthenticated();
      if (access === AccessLevels.user) {
        return this.isAuthenticated();
      } else {
        return true;
      }
    },
    isAuthenticated: function() {
      return localStorage.getItem('auth_token');
    },
    login: function(credentials) {
      console.log(credentials);
      var login = RestService.login(credentials);// $http.post('/auth/authenticate', credentials);
      login.success(function(data, status, headers, config) {
        console.log(headers);
        localStorage.setItem('auth_token', JSON.stringify(headers('X-AUTH-TOKEN')));
        $rootScope.isAuthenticated = true;
        $state.go('main');
      });
      login.error(function(data, status, headers, config) {
        console.log(status);
      });
      return login;
    },
    logout: function() {
      // The backend doesn't care about logouts, delete the token and you're good to go.
      localStorage.removeItem('auth_token');
      $rootScope.isAuthenticated = false;
      $state.go('main');
    },
    register: function(formData) {
      localStorage.removeItem('auth_token');
      var register = $http.post('/auth/register', formData);
      register.success(function(result) {
        localStorage.setItem('auth_token', JSON.stringify(result));
        $rootScope.isAuthenticated = true;
        $state.go('main');
      });
      return register;
    }
  };
}]);
