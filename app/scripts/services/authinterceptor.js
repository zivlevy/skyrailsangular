'use strict';

/**
 * @ngdoc service
 * @name skyRailsApp.AuthInterceptor
 * @description
 * # AuthInterceptor
 * Service in the skyRailsApp.
 */
angular.module('skyRailsApp')
.factory('AuthInterceptor', ['$q', '$injector',function($q, $injector) {
  return {
    request: function(config) {
      var token;
      if (localStorage.getItem('auth_token')) {
        token = angular.fromJson(localStorage.getItem('auth_token'));
      }
      if (token) {
        config.headers['X-AUTH-TOKEN'] = token;
      }
      //config.headers['Content-Type']='application/json';
      return config;
    },
    responseError: function(response) {
      if (response.status === 401 || response.status === 403) {
        localStorage.removeItem('auth_token');
        $injector.get('$state').go('login');
      }
      return $q.reject(response);
    }
  };
}]);
