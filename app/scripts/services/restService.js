'use strict';

/**
 * @ngdoc service
 * @name skyRailsApp.RestServices
 * @description
 * # RestServices for SkyRails
 * Service in the skyRailsApp.
 */

angular.module('skyRailsApp')
  .factory('RestService', ['$http', function($http) {

    var urlBase = 'http://104.155.9.154:8080';
    urlBase = 'http://104.155.47.216:8080';
    var dataFactory = {};
    //$http.defaults.headers.post = {}; // to disable OPTIONS sent in cros
    //$http.defaults.headers.put = {}; // to disable OPTIONS sent in cros
    //$http.defaults.headers.get = {}; // to disable OPTIONS sent in cros
    /// work aginst different hosts

    dataFactory.setHost = function (isLocal) {
      if (isLocal =='local'){
        urlBase = 'http://localhost:8080';
      } else if (isLocal =='fast'){
        urlBase = 'http://104.155.47.216:8080';
      } else if (isLocal =='skyrails'){
        urlBase = 'http://104.155.9.154:8080';
      }else {
        urlBase = 'http://104.155.51.97:8080';
      }
      loginUrlBase = urlBase + '/login';
      droneUrlBase = urlBase + '/drone';
      areaUrlBase = urlBase + '/manualZone';
      ruleUrlBase = urlBase + '/rule';
      riskUrlBase = urlBase + '/riskMap';
      routeUrlBase = urlBase + '/route';
      routeUpdateUrlBase = urlBase + '/routeUpdate';
      lastupdatesUrlBase = urlBase + '/systemCommand/getLastUpdate';
      droneCategoryUrlBase = urlBase + '/droneCategory';
      droneOperatorUrlBase = urlBase + '/droneOperator';
      droneManufacturerUrlBase = urlBase + '/droneManufacturer';
      return ;
    };

    ////////////////////// Login /////////////////////
    var loginUrlBase = urlBase + '/login';
    dataFactory.login = function (credentials) {
      return $http.post(loginUrlBase, credentials);
    };


    ////////////////////// LastUpdates /////////////////////
    var lastupdatesUrlBase = urlBase + '/systemCommand/getLastUpdate';
    dataFactory.getLastUpdate = function () {
      return $http.get(lastupdatesUrlBase);
    };


    ////////////////////// Drones /////////////////////

    var droneUrlBase = urlBase + '/drone';
    dataFactory.getDrone = function () {
      return $http.get(droneUrlBase);
    };

    dataFactory.getDrone = function (id) {
      return $http.get(droneUrlBase + '/' + id);
    };

    dataFactory.insertDrone = function (item) {
      return $http.post(droneUrlBase, item);
    };

    dataFactory.updateDrone = function (id,item) {
      return $http.put(droneUrlBase + '/' + id, item);
    };

    dataFactory.deleteDrone = function (id) {
      return $http.delete(droneUrlBase + '/' + id);
    };

    //dataFactory.getDrones = function (id) {
    //  return $http.get(urlBase + '/' + id + '/orders');
    //};
    dataFactory.queryDrone = function (query) {
      return $http.put(droneUrlBase+'/query',query);
    };

    ////////////////// Areas //////////////////////////
    var areaUrlBase = urlBase + '/manualZone';
    dataFactory.getAreas = function () {
      console.log('Starting GET');
      return $http.get(areaUrlBase);
    };

    dataFactory.getArea = function (id) {
      return $http.get(areaUrlBase + '/' + id);
    };

    dataFactory.insertArea = function (item) {
      console.log(item);
      return $http.post(areaUrlBase, item);
    };

    dataFactory.updateArea = function (id,item) {
      return $http.put(areaUrlBase + '/' + id, item);
    };

    dataFactory.deleteArea = function (id) {
      return $http.delete(areaUrlBase + '/' + id);
    };


    dataFactory.queryArea = function (query) {
      return $http.put(areaUrlBase+'/query',query);
    };

    dataFactory.deleteByQueryArea = function (query) {
      console.log(query);
      return $http.delete(areaUrlBase+'/query',query);
    };
    ////////////////////// Rules /////////////////////

    var ruleUrlBase = urlBase + '/rule';
    dataFactory.getRules = function () {
      console.log(ruleUrlBase);
      return $http.get(ruleUrlBase);
    };

    dataFactory.getRule = function (id) {
      return $http.get(ruleUrlBase + '/' + id);
    };

    dataFactory.insertRule = function (item) {
      return $http.post(ruleUrlBase, item);
    };

    dataFactory.updateRule = function (id,item) {
      return $http.put(ruleUrlBase + '/' + id, item);
    };

    dataFactory.deleteRule = function (id) {
      return $http.delete(ruleUrlBase + '/' + id);
    };

    //dataFactory.getRules = function (id) {
    //  return $http.get(urlBase + '/' + id + '/orders');
    //};
    dataFactory.queryRule = function (query) {
      return $http.put(ruleUrlBase+'/query',query);
    };

    ////////////////////// Risk /////////////////////

    var riskUrlBase = urlBase + '/riskMap';
    dataFactory.getRiskMap = function (resCell,droneID,minLat,minLon,maxLat,maxLon) {

      return $http.get(riskUrlBase + '?res_cell=' + resCell + '&drone_id='+ droneID + '&min_lon=' + minLon + '&min_lat=' + minLat + '&max_lon=' + maxLon + '&max_lat=' + maxLat);
    };


    ////////////////////// Route /////////////////////

    var routeUrlBase = urlBase + '/route';
    dataFactory.getRoute = function (resCell,droneID,from_lat,from_lon,to_lat,to_lon) {

      return $http.get(routeUrlBase + '?res_cell=' + resCell + '&drone_id='+ droneID + '&from_lon=' + from_lon + '&from_lat=' + from_lat + '&to_lon=' + to_lon + '&to_lat=' + to_lat);
    };

      ////////////////////// routeUpdate /////////////////////

      var routeUpdateUrlBase = urlBase + '/routeUpdate';
      dataFactory.getRouteUpdate = function (resCell,droneID,from_lat,from_lon,to_lat,to_lon) {

        return $http.get(routeUpdateUrlBase + '?res_cell=' + resCell + '&drone_id='+ droneID + '&from_lon=' + from_lon + '&from_lat=' + from_lat + '&to_lon=' + to_lon + '&to_lat=' + to_lat);
      };


      dataFactory.getIsPointOpen = function (droneID,Lat,Lon) {

        return $http.get(routeUrlBase + '/isPointOpen?drone_id='+ droneID + '&lon=' + Lon + '&lat=' + Lat);
      };
     ////////////////////// drone Operator /////////////////////

    var droneOperatorUrlBase = urlBase + '/droneOperator';
    dataFactory.getdroneOperators = function () {
      console.log(droneOperatorUrlBase);
      return $http.get(droneOperatorUrlBase);
    };

    dataFactory.getdroneOperator = function (id) {
      return $http.get(droneOperatorUrlBase + '/' + id);
    };

    dataFactory.insertDroneOperator = function (item) {
      return $http.post(droneOperatorUrlBase, item);
    };

    dataFactory.updateDroneOperator = function (id,item) {
      return $http.put(droneOperatorUrlBase + '/' + id, item);
    };

    dataFactory.deleteDroneOperator = function (id) {
      return $http.delete(droneOperatorUrlBase + '/' + id);
    };

    dataFactory.queryDroneOperator = function (query) {
      return $http.put(droneOperatorUrlBase+'/query',query);
    };


    ////////////////////// drone Manufacturer /////////////////////

    var droneManufacturerUrlBase = urlBase + '/droneManufacturer';
    dataFactory.getdroneManufacturer = function () {
      return $http.get(droneManufacturerUrlBase);
    };

    dataFactory.getdroneManufacturer = function (id) {
      return $http.get(droneManufacturerUrlBase + '/' + id);
    };

    dataFactory.insertDroneManufacturer = function (item) {
      return $http.post(droneManufacturerUrlBase, item);
    };

    dataFactory.updateDroneManufacturer = function (id,item) {
      return $http.put(droneManufacturerUrlBase + '/' + id, item);
    };

    dataFactory.deleteDroneManufacturer = function (id) {
      return $http.delete(droneManufacturerUrlBase + '/' + id);
    };

    dataFactory.queryDroneManufacturer = function (query) {
      return $http.put(droneManufacturerUrlBase+'/query',query);
    };

    ////////////////////// drone Category /////////////////////

    var droneCategoryUrlBase = urlBase + '/droneCategory';
    dataFactory.getdroneCategories = function () {
      console.log(droneCategoryUrlBase);
      return $http.get(droneCategoryUrlBase);
    };

    dataFactory.getdroneCategory = function (id) {
      return $http.get(droneCategoryUrlBase + '/' + id);
    };

    dataFactory.insertDroneCategory = function (item) {
      return $http.post(droneCategoryUrlBase, item);
    };

    dataFactory.updateDroneCategory = function (id,item) {
      return $http.put(droneCategoryUrlBase + '/' + id, item);
    };

    dataFactory.deleteDroneCategory = function (id) {
      return $http.delete(droneCategoryUrlBase + '/' + id);
    };

    dataFactory.queryDroneCategory = function (query) {
      return $http.put(droneCategoryUrlBase+'/query',query);
    };

    /////////////////////////////////////////////////////
    return dataFactory;
  }]);


