'use strict';

/**
 * @ngdoc service
 * @name skyRailsApp.sim
 * @description
 * # sim
 * Service in the skyRailsApp.
 */
angular.module('skyRailsApp')
    .service('sim',['$interval','RestService', function ($interval,RestService) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var exposed = {};
 var self = this;
        var originalStart = {latitude :0, longitude:0};
        var originalEnd = {latitude :0, longitude:0};
        var currentLocation = {latitude :0, longitude:0};
        var nextWP = {latitude:0,longitude:0};
        var nextWPNumber;
        var isNextWPLast;
        var currentRoute = [];
        var lastDistanceToNextWP = 10000000;

        var heading =0.0;
        var speedInMetersPerSeconds = 400 * 0.51444444444 ;
        var timeIntervalSeconds = 1;

        var inFlight = false;
        var getInterval;

        var droneID;
        var callBack;
        var callBackNewRoute;

        exposed.stopFly = function() {
             stopInterval();
             originalStart = {latitude :0, longitude:0};
             originalEnd = {latitude :0, longitude:0};
             currentLocation = {latitude :0, longitude:0};
             nextWP = {latitude:0,longitude:0};
             nextWPNumber;
             isNextWPLast;
             currentRoute = [];
             lastDistanceToNextWP = 10000000;


        };
        /*              fly               */
        exposed.fly = function(route,currentdDoneId ,updateCallBack, newRouteCallback){

        if (self.getInterval) {
            stopInterval();
        }
            droneID = currentdDoneId;
            currentRoute = route;
            route = route.reverse();
            originalStart.longitude = route[0][0];
            originalStart.latitude = route[0][1];
            originalEnd.longitude = route[route.length-1][0];
            originalEnd.latitude = route[route.length-1][1];

            currentLocation.latitude = originalStart.latitude;
            currentLocation.longitude = originalStart.longitude;

            nextWP.longitude = route[1][0];
            nextWP.latitude = route[1][1];
            nextWPNumber = 1;
            if (nextWPNumber == route.length -1){
                isNextWPLast = true;
            }  else {
                isNextWPLast = false;
            }

            callBack = updateCallBack;
            callBackNewRoute=newRouteCallback;

            ////set timer to 1 sec
            self.getInterval = $interval(function () {
                simStep();
            }, timeIntervalSeconds * 1000);

        };


        // Interval
        var droneLastUpdate =0;
        var rulesLastUpdate=0;
        var zonesLastUpdate=0;
        var isUpdateNeeded = false;

        /*       simStep             */
        var simStep = function () {
            console.log("tick");
            //move from current point twords next point
            var bearing =  (getBearing(currentLocation.latitude,currentLocation.longitude,nextWP.latitude,nextWP.longitude));

            var pointA = new google.maps.LatLng(currentLocation.latitude, currentLocation.longitude);
            var pointB = pointA.destinationPoint (bearing, timeIntervalSeconds * speedInMetersPerSeconds / 1000);

            currentLocation.latitude = pointB.lat();
            currentLocation.longitude = pointB.lng();
            var distanceToNextPoint = getDistace(currentLocation.latitude,currentLocation.longitude, nextWP.latitude,nextWP.longitude);

            if (distanceToNextPoint <=0.25 || lastDistanceToNextWP < distanceToNextPoint){
                if (isNextWPLast){
                    console.log("Landing");

                    currentLocation.latitude = originalEnd;
                    currentLocation.longitude=currentRoute[nextWPNumber][0];
                    currentLocation.latitude=currentRoute[nextWPNumber][1];

                    lastDistanceToNextWP = 10000000;
                    stopInterval();

                }
                else  {
                    console.log("nextWP:" + nextWPNumber);
                    currentLocation.longitude=currentRoute[nextWPNumber][0];
                    currentLocation.latitude=currentRoute[nextWPNumber][1];
                    nextWPNumber+=1;
                    if (nextWPNumber == currentRoute.length -1){
                        isNextWPLast = true;
                    }  else {
                        isNextWPLast = false;
                    }
                    nextWP.longitude = currentRoute[nextWPNumber][0];
                    nextWP.latitude = currentRoute[nextWPNumber][1];
                    lastDistanceToNextWP = 10000000;
                }
            } else {
                lastDistanceToNextWP = distanceToNextPoint;
            }
            callBack(currentLocation);


            ///////////// check server updates /////////////////
            RestService.getLastUpdate().then(
                // good response
                function (response) {
                    var isUpdateNeeded = false;
                    if (rulesLastUpdate < response.data.ruleLastUpdate || zonesLastUpdate<response.data.zoneLastUpdate){
                        rulesLastUpdate = response.data.ruleLastUpdate;
                        zonesLastUpdate = response.data.zoneLastUpdate;
                        isUpdateNeeded = true;
                    }


                    if (isUpdateNeeded) {
                        getRoute();

                    }

                },

                //error on response
                function (errorResponse) {

                });
        };

///////////////////////
        var inCall = false;
        var getRoute = function () {
                console.log("in here");
                if (inCall) {
                    return;
                }
                inCall = true;
                var resolution = 30;
                RestService.getRouteUpdate (resolution, droneID.id, currentLocation.latitude, currentLocation.longitude, originalEnd.latitude, originalEnd.longitude).then(
                    // good response
                    function (response) {

                        inCall = false;
                        var allObjects = response.data;

                        callBackNewRoute(allObjects);

                        //setup new nav data
                        currentRoute = allObjects.geometry.coordinates.slice();
                        lastDistanceToNextWP = 10000000;
                        exposed.fly(currentRoute,droneID,callBack,callBackNewRoute);

                    },
                    //error on response
                    function (errorResponse) {
                        inCall = false;
                    });
            };

            //////////////////////

        var stopInterval =  function() {
            console.log("stop interval");
            // Make sure that the interval is destroyed too
            $interval.cancel(self.getInterval);
            self.getInterval = undefined;
        };


        function radians(n) {
            return n * (Math.PI / 180);
        }
        function degrees(n) {
            return n * (180 / Math.PI);
        }

        function getBearing(startLat,startLong,endLat,endLong){
            startLat = radians(startLat);
            startLong = radians(startLong);
            endLat = radians(endLat);
            endLong = radians(endLong);

            var dLong = endLong - startLong;

            var dPhi = Math.log(Math.tan(endLat/2.0+Math.PI/4.0)/Math.tan(startLat/2.0+Math.PI/4.0));
            if (Math.abs(dLong) > Math.PI){
                if (dLong > 0.0)
                    dLong = -(2.0 * Math.PI - dLong);
                else
                    dLong = (2.0 * Math.PI + dLong);
            }

            return (degrees(Math.atan2(dLong, dPhi)) + 360.0) % 360.0;
        }

        function getDistace(lat1,lon1,lat2,lon2) {
            var R = 6371; // km
            var dLat = (lat2-lat1).toRad();
            var dLon = (lon2-lon1).toRad();
            var lat1 = lat1.toRad();
            var lat2 = lat2.toRad();

            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c ;
            return d ;
        }

        Number.prototype.toRad = function() {
            return this * Math.PI / 180;
        };

        Number.prototype.toDeg = function() {
            return this * 180 / Math.PI;
        };

        google.maps.LatLng.prototype.destinationPoint = function(brng, dist) {
            dist = dist / 6371;
            brng = brng.toRad();

            var lat1 = this.lat().toRad(), lon1 = this.lng().toRad();

            var lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist) +
                Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));

            var lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(dist) *
                    Math.cos(lat1),
                    Math.cos(dist) - Math.sin(lat1) *
                    Math.sin(lat2));

            if (isNaN(lat2) || isNaN(lon2)) return null;

            return new google.maps.LatLng(lat2.toDeg(), lon2.toDeg());
        };

        ////////////////////////////
        return exposed;



    }]);
