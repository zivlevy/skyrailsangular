'use strict';

describe('Controller: AreaqueryCtrl', function () {

  // load the controller's module
  beforeEach(module('skyRailsApp'));

  var AreaqueryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AreaqueryCtrl = $controller('AreaqueryCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
