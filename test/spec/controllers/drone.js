'use strict';

describe('Controller: DroneCtrl', function () {

  // load the controller's module
  beforeEach(module('skyRailsApp'));

  var DroneCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DroneCtrl = $controller('DroneCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
