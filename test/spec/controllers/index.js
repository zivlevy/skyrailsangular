'use strict';

describe('Controller: IndexctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('skyRailsApp'));

  var IndexctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IndexctrlCtrl = $controller('IndexctrlCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
