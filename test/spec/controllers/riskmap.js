'use strict';

describe('Controller: RiskmapCtrl', function () {

  // load the controller's module
  beforeEach(module('skyRailsApp'));

  var RiskmapCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RiskmapCtrl = $controller('RiskmapCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
