'use strict';

describe('Controller: RouteplanerCtrl', function () {

  // load the controller's module
  beforeEach(module('skyRailsApp'));

  var RouteplanerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RouteplanerCtrl = $controller('RouteplanerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
