'use strict';

describe('Controller: SearchandrescueCtrl', function () {

  // load the controller's module
  beforeEach(module('skyRailsApp'));

  var SearchandrescueCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SearchandrescueCtrl = $controller('SearchandrescueCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
