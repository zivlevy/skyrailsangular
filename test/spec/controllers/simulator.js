'use strict';

describe('Controller: SimulatorCtrl', function () {

  // load the controller's module
  beforeEach(module('skyRailsApp'));

  var SimulatorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SimulatorCtrl = $controller('SimulatorCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SimulatorCtrl.awesomeThings.length).toBe(3);
  });
});
