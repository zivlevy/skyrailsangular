'use strict';

describe('Directive: addressSearch', function () {

  // load the directive's module
  beforeEach(module('skyRailsApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<address-search></address-search>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the addressSearch directive');
  }));
});
