'use strict';

describe('Service: ManualArea', function () {

  // load the service's module
  beforeEach(module('skyRailsApp'));

  // instantiate service
  var ManualArea;
  beforeEach(inject(function (_ManualArea_) {
    ManualArea = _ManualArea_;
  }));

  it('should do something', function () {
    expect(!!ManualArea).toBe(true);
  });

});
